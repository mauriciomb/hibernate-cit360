package com.cit360.hibernate.demo.service;

import com.cit360.hibernate.demo.entity.StateCapital;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface StateCapitalService extends JpaRepository<StateCapital, Long> {

    StateCapital findByStateID(String state);
}
