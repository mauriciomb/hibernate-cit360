package com.cit360.hibernate.demo.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.StringJoiner;

@Entity
@Table(name = "state_capital")
public class StateCapital implements Serializable {

    private static final long serialVersionUID = -6114109084786235740L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    private Integer employeeId;

    @Column(name = "stateID", unique = true, nullable = false, length = 100)
    private String stateID;

    @Column(name = "capital", unique = true, nullable = false, length = 100)
    private String capital;

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getStateID() {
        return stateID;
    }

    public void setStateID(String stateID) {
        this.stateID = stateID;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StateCapital that = (StateCapital) o;

        if (employeeId != null ? !employeeId.equals(that.employeeId) : that.employeeId != null) return false;
        if (stateID != null ? !stateID.equals(that.stateID) : that.stateID != null) return false;
        return capital != null ? capital.equals(that.capital) : that.capital == null;
    }

    @Override
    public int hashCode() {
        int result = employeeId != null ? employeeId.hashCode() : 0;
        result = 31 * result + (stateID != null ? stateID.hashCode() : 0);
        result = 31 * result + (capital != null ? capital.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", StateCapital.class.getSimpleName() + "[", "]")
                .add("employeeId=" + employeeId)
                .add("stateID='" + stateID + "'")
                .add("capital='" + capital + "'")
                .toString();
    }
}
