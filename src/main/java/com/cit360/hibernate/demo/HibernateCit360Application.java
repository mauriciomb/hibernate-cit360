package com.cit360.hibernate.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HibernateCit360Application {

	public static void main(String[] args) {
		SpringApplication.run(HibernateCit360Application.class, args);
	}

}
