package com.cit360.hibernate.demo;

import com.cit360.hibernate.demo.entity.StateCapital;
import com.cit360.hibernate.demo.service.StateCapitalService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class HibernateCit360ApplicationTests {

	@Autowired
	private StateCapitalService stateCapitalService;

	@Test
	void contextLoads() {
		List<StateCapital> list = stateCapitalService.findAll();
		for (StateCapital stateCapital : list) {
			System.out.println(stateCapital.toString());
		}
	}

	@Test
	void findSpecificCapital() {
		StateCapital stateCapital = stateCapitalService.findByStateID("AZ");
		System.out.println(stateCapital.toString());
	}

}
